package com.example.mvvm_hilt_retrofit.di

import com.example.mvvm_hilt_retrofit.data.MainRepository
import com.example.mvvm_hilt_retrofit.data.remote.ApiService
import com.example.mvvm_hilt_retrofit.data.remote.MainRepoRemoteImpl
import com.example.mvvm_hilt_retrofit.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Named("remote")
    fun provideRemoteRepository(mainRepoRemoteImpl: MainRepoRemoteImpl): MainRepository {
        return mainRepoRemoteImpl
    }

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun provideRetrofitClient(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}