package com.example.mvvm_hilt_retrofit.views


import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvm_hilt_retrofit.adapter.BookAdapter
import com.example.mvvm_hilt_retrofit.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

private const val TAG = "MainActivity"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainActivityViewModel: MainActivityViewModel
    lateinit var binding: ActivityMainBinding
    private lateinit var adapter: BookAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mainActivityViewModel.getBooksFromRemoteSource()

        binding.bookRecyclerView.layoutManager = LinearLayoutManager(this)
        mainActivityViewModel.bookList.observe(this) {
            Log.d(TAG, "onCreate: " + it.size)
            adapter = BookAdapter(mainActivityViewModel.bookList.value!!)
            binding.bookRecyclerView.adapter = adapter
            binding.bookRecyclerView.adapter?.notifyDataSetChanged()
        }


    }

}