package com.example.mvvm_hilt_retrofit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm_hilt_retrofit.R
import com.example.mvvm_hilt_retrofit.data.models.Book


class BookAdapter(private val bookList: List<Book>) :
    RecyclerView.Adapter<BookAdapter.BookViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_book, parent, false)
        return BookViewHolder(view)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val book = bookList[position]
        holder.bookNameView.setText(book.name)
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

     class BookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var bookNameView: TextView

        init {
            bookNameView = itemView.findViewById<TextView>(R.id.bookNameView)
        }
    }
}