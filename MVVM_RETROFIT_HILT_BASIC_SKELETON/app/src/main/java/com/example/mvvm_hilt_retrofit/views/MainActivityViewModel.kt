package com.example.mvvm_hilt_retrofit.views

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvvm_hilt_retrofit.data.MainRepository
import com.example.mvvm_hilt_retrofit.data.models.Book
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

private const val TAG = "MainActivityViewModel"

class MainActivityViewModel @Inject constructor( @Named("remote") private val mainRepository: MainRepository): ViewModel() {


    private val _bookList = MutableLiveData<List<Book>>()
    val bookList: LiveData<List<Book>>
    get() = _bookList
    private val tmpBooks = ArrayList<Book>();



    fun getBooksFromRemoteSource(){
        viewModelScope.launch {
            try {
                tmpBooks.clear()
                mainRepository.getBooks()?.forEach { book ->
                    tmpBooks.add(book);
                }
                if(tmpBooks.size==0){
                    //No book found
                }else{
                    _bookList.postValue(tmpBooks);
                }
            }catch (e: java.lang.Exception){
                Log.d(TAG, "testVm: "+ e.localizedMessage)
            }
        }
    }
}