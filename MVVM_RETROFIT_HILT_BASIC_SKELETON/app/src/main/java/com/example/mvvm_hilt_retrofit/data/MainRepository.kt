package com.example.mvvm_hilt_retrofit.data

import com.example.mvvm_hilt_retrofit.data.models.Book

interface MainRepository {
    suspend fun getBooks(): List<Book>?
    suspend fun getSingleBook(id: Int): Book?
}