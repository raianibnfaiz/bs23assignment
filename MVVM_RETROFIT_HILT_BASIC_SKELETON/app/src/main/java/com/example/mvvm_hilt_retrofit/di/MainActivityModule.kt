package com.example.mvvm_hilt_retrofit.di

import com.example.mvvm_hilt_retrofit.databinding.ActivityMainBinding
import com.example.mvvm_hilt_retrofit.views.MainActivity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class MainActivityModule {

}