package com.example.mvvm_hilt_retrofit.data.remote

import com.example.mvvm_hilt_retrofit.data.models.Book
import javax.inject.Inject

private const val TAG = "RemoteDataSource"

class RemoteDataSource @Inject constructor (private val apiService: ApiService) {
    suspend fun getAllBooks(): List<Book>?{
        return apiService.getAllBooks()
    }
    suspend fun getSignelBook(id: Int): Book?{
        return apiService.getSingleBook(id)
        ;
    }
}