package com.example.mvvm_hilt_retrofit.data.remote

import com.example.mvvm_hilt_retrofit.data.models.Book
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
   @GET("/book-list")
   suspend fun getAllBooks(): List<Book>?
   @GET("/book-list")
   suspend fun getSingleBook(@Query("id") id: Int): Book?
}