package com.example.mvvm_hilt_retrofit.data.models

data class Book(
    val id: Int,
    val image: String,
    val name: String,
    val url: String
)