package com.example.mvvm_hilt_retrofit.views

import android.net.Uri
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.mvvm_hilt_retrofit.R
import com.example.mvvm_hilt_retrofit.databinding.ActivityBookDetailBinding

class BookDetailActivity : AppCompatActivity() {
    lateinit var binding: ActivityBookDetailBinding
    private val viewModel: BookDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.getSingleBook(intent.getIntExtra("id", 0))
        viewModel.book.observe(this) {
            if(it!=null){
                binding.bookImageView.setImageURI(Uri.parse(it.image))
                binding.bookTitleTextView.text = it.name
            }
        }
    }
}