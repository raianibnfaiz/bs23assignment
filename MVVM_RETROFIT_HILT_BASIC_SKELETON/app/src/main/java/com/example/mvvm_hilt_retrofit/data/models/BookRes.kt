package com.example.mvvm_hilt_retrofit.data.models

data class BookRes(
    val books: List<Book>,
    val message: String,
    val status: Int
)