package com.example.mvvm_hilt_retrofit.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvvm_hilt_retrofit.data.MainRepository
import com.example.mvvm_hilt_retrofit.data.models.Book
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named
class BookDetailViewModel @Inject constructor( @Named("remote") private val remoteRepo: MainRepository): ViewModel() {
    private val _book = MutableLiveData<Book>()
    val book: LiveData<Book>
        get() = _book


    fun getSingleBook(id: Int) {
        viewModelScope.launch {
            try {
               _book.postValue(remoteRepo.getSingleBook(id))
            }catch (e:Exception){

            }
        }
    }
}