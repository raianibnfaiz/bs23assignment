package com.example.mvvm_hilt_retrofit.data.remote

import com.example.mvvm_hilt_retrofit.data.MainRepository
import com.example.mvvm_hilt_retrofit.data.models.Book
import javax.inject.Inject

private const val TAG = "MainRepoRemoteImpl"

class MainRepoRemoteImpl @Inject constructor(private val remoreDataSource: RemoteDataSource):
    MainRepository {

    override suspend fun getBooks(): List<Book>? {
        return remoreDataSource.getAllBooks()
    }
    override suspend fun getSingleBook(id: Int): Book? {
        return remoreDataSource.getSignelBook(id)

    }
}